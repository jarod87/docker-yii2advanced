# README #

Yii2 Advanced Docker Wrapper

* nginx 1.11.3
* php-fpm 7.1
* postgres 9.6
* composer php7 (just build)
* node 6.5.0 (just build)



```bash
git clone git@bitbucket.org:jarod87/docker-yii2advanced.git

composer global require "fxp/composer-asset-plugin:^1.2.0"

# basic
composer create-project --prefer-dist yiisoft/yii2-app-basic php/src

# advanced
composer create-project --prefer-dist yiisoft/yii2-app-advanced php/src

docker-compose build
docker-compose up

OR

docker-compose up --build
```

phppgAdmin: [localhost:8888](http://localhost:8888)


### Advanced Settings ###
example **mysite** and **admin.mysite** url
```bash
echo "$(docker inspect -f '{{ .NetworkSettings.Networks.dockeryii2advanced_default.Gateway }}' php) mysite admin.mysite" | sudo tee -a /etc/hosts > /dev/null

```

Frontend: [http://mysite/](http://mysite)

Backend: [http://admin.mysite/](http://admin.mysite)


### Dependencies! ###

* [Docker && Docker-Compose (Ubuntu 16.04)](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-16-04)
* [Docker && Docker-Compose (Ubuntu 14.04)](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-compose-on-ubuntu-14-04)